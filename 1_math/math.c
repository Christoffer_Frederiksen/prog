#include<stdio.h>
#include<math.h>
#include<tgmath.h>
#include<complex.h>
int main(){
	printf("\n\n Exercise 1:\n\n");
	printf("Gamma function to 5: %g \n", tgamma(5));
	printf("Bessel function to 0.5: %g \n",j0(0.5));
	complex ans=csqrt(-2);
	printf("The squere root of minus 2: %g+%g*i\n", creal(ans),cimag(ans));
	complex ans1=cexp(I);
	printf("exp(i)=%g+%g*i\n", creal(ans1),cimag(ans1));
	double complex ans2=cexp(I*M_PI);
        printf("exp(i*pi)=%g+%lg*i\n", creal(ans2),cimag(ans2));
	complex ans3=pow(I,cexp(1));
        printf("i to the power of e: %g+%g*i\n", creal(ans3),cimag(ans3));
	printf("\n\n Exercise 2: \n\n");
	float x =0.111111111111111111111111111111;
	double y =0.111111111111111111111111111111;
	long double z =0.111111111111111111111111111111;
	printf("Same number stored ind float, double and long double, respectivly:\n %.25g\n %.25lg\n %.25Lg\n\n", x,y,z);
return 0;
}
