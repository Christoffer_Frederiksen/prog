#include<math.h>
#include<gsl/gsl_integration.h>
#include<gsl/gsl_errno.h>
#include<stdio.h>

double integrand(double x, void* params){
	return 1/(x*x+1);
}

double my_arctan(double x){
	if(x<0) return -my_arctan(-x);
	if(x>1) return M_PI/2-my_arctan(1/x);
 
	double a=0,b=x,acc=1e-6,eps=1e-6,result,err;
	int limit = 1000;

	gsl_function f;
	f.function = integrand;
	f.params = NULL;

	gsl_integration_workspace * workspace =gsl_integration_workspace_alloc(limit);

	int status=gsl_integration_qags(&f,a,b,acc,eps,limit,workspace,&result,&err);

	gsl_integration_workspace_free(workspace);

	if(status==GSL_SUCCESS)	return result;
	else return NAN;
}

int main(){
	double a=-100, b=100, dx=0.2;
	for(double x=a; x<=b;x+=dx) printf("%g %g %g\n", x, my_arctan(x),atan(x));
return 0;
}

