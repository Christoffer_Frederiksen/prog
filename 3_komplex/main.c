#include "komplex.h"
#include <stdio.h>
#include<complex.h>
#include<tgmath.h>

int main()
{	
	printf("\n\n Testing komplex_set and printing.\n");
	komplex z;	
	komplex *pointer = &z;
	komplex_set(pointer,2,4);
	komplex_print("z=",z);
	printf("\n Changing numbers in memory and printing using pointer\n");
	komplex_set(pointer,3,1);
	komplex_print("z=",z);

	printf("\n Defining a and b as komplex numbers with komplex_new and testing komplex_print:\n");
	komplex a=komplex_new(1,2);
	komplex b=komplex_new(3,4);
	komplex_print("a=",a);
	komplex_print("b=",b);

	printf("\n\n testing komplex_add\n");
	komplex r = komplex_add(a,b);
	komplex R = {4,6};		
	komplex_print("a+b should   = ", R);
	komplex_print("a+b actually = ", r);
	
	printf("\n\n testing komplex_sub\n");
	komplex d = komplex_sub(a,b);
	komplex D = {-2,-2};
	komplex_print("a-b should = ", D);
	komplex_print("a-b actually = ",d);	
return 0;
}
