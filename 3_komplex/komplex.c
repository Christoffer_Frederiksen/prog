#include "komplex.h"
#include <stdio.h>

void komplex_print(char* s, komplex z){
	if(z.im<0){
	printf("%s%g%g*i\n", s, z.re,z.im);
	}
	else{
	printf("%s%g+%g*i\n", s, z.re,z.im);
	}
}

void    komplex_set(komplex* z, double x, double y){
	(*z).re = x;
	(*z).im = y;
} 	   /* z ← x+iy */

komplex komplex_new(double x , double y ){
	komplex z = {x,y};
	return z;
}          /* returns x+iy */

komplex komplex_add(komplex a, komplex b){
	komplex z = {a.re + b.re , a.im + b.im};
	return z;
}         /* returns a+b */

komplex komplex_sub(komplex a, komplex b){
	komplex z = {a.re - b.re , a.im - b.im};
	return z;
}         /* returns a-b */

