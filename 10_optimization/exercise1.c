#include<gsl/gsl_multimin.h>
#include<assert.h>

struct experimental_data {int n; double *t,*y,*e;};

double func(const gsl_vector *v, void *params) {
	double x=gsl_vector_get(v,0);
	double y=gsl_vector_get(v,1);
	return pow((1-x),2)+100*pow((y-x*x),2);
}

int main(){
	gsl_multimin_function f;
	f.f=func;
	f.n=2;
	f.params=NULL;

	gsl_multimin_fminimizer *state 
=gsl_multimin_fminimizer_alloc(gsl_multimin_fminimizer_nmsimplex2,2);
	gsl_vector *start = gsl_vector_alloc(2);
	gsl_vector *step = gsl_vector_alloc(2);
	gsl_vector_set(start,0,0.5); 	
	gsl_vector_set(start,1,0.5); 
	gsl_vector_set_all(step,0.1);
	gsl_multimin_fminimizer_set (state, &f, start, step);

	int iter=0,status;
	double acc=0.001;
	do{
	iter++;
	int flag = gsl_multimin_fminimizer_iterate (state);
	if(flag!=0)break;
	status = gsl_multimin_test_size (state->size, acc);
	}while(status == GSL_CONTINUE && iter < 99);
	
	double x = gsl_vector_get(state->x,0);
	double y = gsl_vector_get(state->x,1);
	printf("\n\n Exercise 1:\n\n");
	printf("Numerical minimum (x,y)=(%g,%g)\n",x,y);
	printf("Analytic minimum (x,y)=(1,1)\n\n");
	gsl_vector_free(start);
	gsl_vector_free(step);
	gsl_multimin_fminimizer_free(state);
return 0;
}
