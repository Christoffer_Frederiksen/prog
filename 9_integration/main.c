#include<math.h>
#include<gsl/gsl_integration.h>
#include<gsl/gsl_errno.h>
#include<stdio.h>
double norm_integrand(double x, void* params){
	double c = *(double*)params;
	return exp(-c*x*x);
}
double ham_integrand(double x, void* params){
	double c = *(double*)params;
	return (-c*c*x*x/2+c/2+x*x/2)*exp(-c*x*x);
}
double integration(double c){
	gsl_function n;
	n.function = norm_integrand;
	n.params = (void*)&c;
	
	gsl_function h;
	h.function = ham_integrand;
	h.params = (void*)&c;

	int limit = 1000;
	double acc=1e-6,eps=1e-6,resultn,resulth,err;
	gsl_integration_workspace * workspace =gsl_integration_workspace_alloc(limit);

	int statusn=gsl_integration_qagi(&n,acc,eps,limit,workspace,&resultn,&err);

	int statush=gsl_integration_qagi(&h,acc,eps,limit,workspace,&resulth,&err);
	
	gsl_integration_workspace_free(workspace);

	if(statusn == GSL_SUCCESS && statush == GSL_SUCCESS) return resulth/resultn;
	else return NAN;
}
int main(){
	double a=0.1, b=2, dx=0.01;
	for(double x=a;x<b;x+=dx) printf("%g %g\n",x,integration(x));
return 0;
}
