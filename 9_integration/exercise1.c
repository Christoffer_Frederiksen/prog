#include<math.h>
#include<gsl/gsl_integration.h>
#include<gsl/gsl_errno.h>
#include<stdio.h>

double integrand(double x, void* params){
	return log(x)/sqrt(x);
}

int main(){
	double a=0,b=1,acc=1e-6,eps=1e-6,result,err=0.01;
	int limit = 1000;

	gsl_function f;
	f.function = &integrand;

	gsl_integration_workspace * workspace =gsl_integration_workspace_alloc(limit);
	int status=gsl_integration_qags(&f,a,b,acc,eps,limit,workspace,&result,&err);
	gsl_integration_workspace_free(workspace);

	if(status==GSL_SUCCESS)	printf("\n\n Exercise 1:\n\n Integral calculated nummerical and the result is: %g\n\n",result);
	else printf("Intergration failed");
return 0;
}


