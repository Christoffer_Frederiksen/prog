#include <stdlib.h>
int equal(double a, double b, double tau, double epsilon){
	int res;
	if(abs(a-b)<tau || abs(a-b)/(abs(a)+abs(b))<epsilon) res=1; 
	else res=0;
	return res;
}
