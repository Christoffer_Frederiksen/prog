#include <stdio.h>
#include "maximum.h"
#include <limits.h>
#include "minimum.h"
#include <float.h>
#include "machineepsilon.h"
#include "sum.h"
#include "equal.h"

int main(){
	printf("\n\nOpgave 1:\n\n");
	printf("Max integer vha. limits: INT_MAX=%i\n", INT_MAX);
	printf("Max integer vha. whileloop: %i\n",intmaxw());
	printf("Max integer vha. if og for loop: %i\n", intmaxif());
	printf("Max integer vha. do-while loop: %i\n", intmaxwd());
	
	printf("Min integer vha. limits: INT_MIN=%i\n", INT_MIN);
	printf("Min integer vha. whileloop: %i\n",intminw());
	printf("Min integer vha. if og for loop: %i\n", intminif());
	printf("Min integer vha. do-while loop: %i\n", intminwd());

	printf("Machine epsilon med float: FLT_EPSILON=%g\n", FLT_EPSILON);
	printf("Machine epsilon med double: DBL_EPSILON=%g\n", DBL_EPSILON);
	printf("Machine epsilon med long double: LDBL_EPSILON=%Lg\n", LDBL_EPSILON);
	printf("Machine epsilon med float og while loop: %g\n", mewf());
	printf("Machine epsilon med double og while loop: %g\n", mew());
	printf("Machine epsilon med long double og while loop: %Lg\n", mewl());
	printf("Machine epsilon med double og for loop: %g\n", mef());
	printf("Machine epsilon med double og do-while loop: %g\n", medw());

	printf("\n\nOpgave 2:\n\n");
	printf("Med max=int_max/3\n");
	printf("Sum up med float: %f\n",sumup());
	printf("Sum down med float: %f\n",sumdown());
	printf("Sum up med double: %g\n",sumupdouble());
	printf("Sum down med double: %g\n",sumdowndouble());

	printf("\n\nOpgave 3:\n\n");
	printf("Returing 1 for a and b equal with absolute precision tau or relative precision eps else 0 is returned\n");
	printf("Testing for (a,b,tau,eps)=(10,5,0,20)=>");
	printf("%i\n",equal(10,5,0,20));
	printf("Testing for (a,b,tau,eps)=(10,5,20,0)=>");
	printf("%i\n",equal(10,5,20,0));
	printf("Testing for (a,b,tau,eps)=(10,5,0,0)=>");
	printf("%i\n",equal(10,5,0,0));
return 0;
}
