#include <limits.h>

int max=INT_MAX/3;


float sumup(){
	float sum = 0;
	for(int n=1; n<=max ;n++){
		sum+=1.0f/n;
	}
return sum; 
}

float sumdown(){
	float sum = 0;
	for(int n=max; n>0 ;n--){
		sum+=1.0f/n;
	}
return sum; 
}

double sumupdouble(){
	double sum = 0;
	for(int n=1; n<=max ;n++){
		sum+=1.0/n;
	}
return sum; 
}

double sumdowndouble(){
	double sum = 0;
	for(int n=max; n>0 ;n--){
		sum+=1.0/n;
	}
return sum; 
}

