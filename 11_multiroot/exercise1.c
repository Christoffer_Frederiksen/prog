#include <stdlib.h>
#include <stdio.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_multiroots.h>

struct rparams
{
	double a;
	double b;
};
int rosenbrock (const gsl_vector * x, void *params, gsl_vector * f){
	double a = ((struct rparams *) params)->a;
  	double b = ((struct rparams *) params)->b;

	const double x0 = gsl_vector_get (x, 0);
	const double x1 = gsl_vector_get (x, 1);

	const double y0 = a * (1 - x0);
	const double y1 = b * (x1 - x0 * x0);

	gsl_vector_set (f, 0, y0);
	gsl_vector_set (f, 1, y1);

return GSL_SUCCESS;
}
int main(){
	const gsl_multiroot_fsolver_type *T;
	gsl_multiroot_fsolver *s;
	int n = 2;
	struct rparams p = {1.0, 100.0};
	gsl_multiroot_function f = {&rosenbrock, n, &p};
	gsl_vector *x = gsl_vector_alloc (n);
	gsl_vector_set (x, 0, 0.5);
	gsl_vector_set (x, 1, 0.5);
	T = gsl_multiroot_fsolver_hybrids;
	s = gsl_multiroot_fsolver_alloc (T, 2);
	gsl_multiroot_fsolver_set (s, &f, x);
	int status, iter=0;
	do
	{
    		iter++;
      		status = gsl_multiroot_fsolver_iterate (s);
      		if (status) break;
		status = gsl_multiroot_test_residual (s->f, 1e-7);
	} 
	while (status == GSL_CONTINUE && iter < 1000);
	printf("\n\nExercise 1\n\n");
	printf("Nummerical minimum found at (x,y)=(%g,%g)\n", gsl_vector_get (s->x, 0), gsl_vector_get (s->x, 1));
	printf("Analytical minimum is (x,y)=(1,1)\n\n");
	gsl_multiroot_fsolver_free (s);
	gsl_vector_free (x);
return 0;
}
