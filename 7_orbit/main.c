#include<stdio.h>
#include<getopt.h>
#include<math.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_odeiv2.h>

int ODE(double t, const double y[], double dydt[], void * params)
{
	double epsilon = *(double *) params;
	dydt[0]=y[1];
	dydt[1]=1-y[0]+epsilon*y[0]*y[0];
return GSL_SUCCESS;
}

int main (int argc, char **argv) {
	double epsilon = 0, uprime = 0;

	while(1){
		int opt = getopt(argc, argv, "e:p:");

		if(opt == -1) break;
	
		switch(opt){
			case 'e': epsilon=atof(optarg); break;
			case 'p': uprime=atof(optarg); break;
		default: 
                	fprintf(stderr, "Usage: %s [-e epsilon] \
			 [-p uprime]\n", argv[0]);
                exit(EXIT_FAILURE);
		}
	}

	gsl_odeiv2_system sys;
	sys.function = ODE;
	sys.jacobian = NULL;
	sys.dimension = 2;
	sys.params = (void*) &epsilon;

	double acc=1e-6, eps=1e-6, hstart=1e-3;
	gsl_odeiv2_driver *driver=gsl_odeiv2_driver_alloc_y_new(&sys,gsl_odeiv2_step_rkf45,hstart,acc,eps);

	double t=0,y[2]={1, uprime};
	double a=0,b=100,dx=0.01;
	
	for(double x=a;x<b+dx;x+=dx){
		gsl_odeiv2_driver_apply(driver, &t, x, y);
		printf("%g %g\n",x,y[0]);
	}
	
	return 0;
}
