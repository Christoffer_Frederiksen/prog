#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_eigen.h>
#include <gsl/gsl_linalg.h>

int main(){
	double a_data[] = { 6.13, -2.90, 5.86,
			    8.09, -6.31, -3.89,
			   -4.36, 1.00, 0.19 };
	double b_data[] = { 6.23, 5.37, 2.29};

	gsl_matrix_view A = gsl_matrix_view_array (a_data, 3, 3);
	gsl_vector_view b = gsl_vector_view_array (b_data, 3);

	gsl_vector *x = gsl_vector_alloc (3);
	gsl_permutation * p = gsl_permutation_alloc (3);
	
	int k;
	gsl_linalg_LU_decomp (&A.matrix, p, &k);
	gsl_linalg_LU_solve (&A.matrix, p, &b.vector, x);
	
	printf("Solution to linear system:\n");
	printf("x = \n");
	gsl_vector_fprintf (stdout, x, "%g");
	printf("Solution found in MatLab as well and the result was the same.\n\n");
	
	gsl_permutation_free (p);
	gsl_vector_free (x);
return 0;
}
