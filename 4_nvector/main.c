#include "nvector.h"
#include <stdio.h>
#include <stdlib.h>

int main(){
	printf("\n\n testing nvector_alloc ...\n");
	
	int n = 3;	
	nvector *v = nvector_alloc(n);	
	nvector *u = nvector_alloc(n);

	if (v == NULL && u == NULL) printf("test failed\n");
	else{
	printf("test passed\n");	
	};

	printf("\n main: testing nvector_set and nvector_get ...\n");
	int v_i, u_i;
	for(int i=0; i<n; i++){
	 	v_i=rand() % 5;
		u_i=rand() % 5;
		nvector_set(v, i, v_i);
		nvector_set(u, i, u_i);
	}
	for(int i=0; i<n; i++) printf("v: %g\n", nvector_get(v,i) );	
	printf("\n");
	for(int i=0; i<n; i++) printf("u: %g\n", nvector_get(u,i) );	
	printf("Dot product of u and v: %g\n", nvector_dot_product(v,u) );
	nvector_free(v);
	nvector_free(u);
return 0;
}
